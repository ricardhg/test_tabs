import React, {useState} from 'react';
import styled from 'styled-components';


const TabsContainer = styled.div`
  display: flex;
  flex-direction: column;
  width: 100%;
`;

const TabWrapper = styled.div`
  flex: auto;
  cursor: pointer;
  padding: 8px;
  border-bottom: ${props => props.active ? "2px solid #0095F2" : "2px solid #8A959E" };
`;

const Title= styled.div`
      color: ${props => props.active ? "#0095F2" : "#8A959E"};
      font-size: 13px;
      font-weight: 600;
      text-align: center;
`;

const TabsNav= styled.div`
  display: flex;
`;

const Content= styled.div`
  padding: 16px 8px;
  font-size: 14px;
  color: #4C5769;
`;


const Tab = ({title, index, activeTab, setActiveTab}) => {
    const active = activeTab===index ? {active: true} : null;
    return (
      <TabWrapper {...active} onClick={()=>setActiveTab(index)}>
        <Title {...active}>{title}</Title>
      </TabWrapper>
    );
  }


export default ({tabs}) => {

  const [activeTab, setActiveTab] = useState(0);

  const tabsNav = tabs.map((item,index) => 
          <Tab 
            key={index}
            title={item.title} 
            index={index} 
            activeTab={activeTab} 
            setActiveTab={setActiveTab} 
          />);

    return (
      <TabsContainer>
        <TabsNav>
          {tabsNav}
        </TabsNav>
        <Content>
          {tabs[activeTab].content}
        </Content>
      </TabsContainer>
    );

}
