import React from 'react';
import Tabs from './Tabs';

const TABS = [
  {title: "Tab1", content: "Content of tab 1"},
  {title: "Tab2", content: "Content of tab 2"},
  {title: "Tab3", content: "Content of tab 3"}
];

export default () => ( 
    <Tabs tabs={TABS} />
);

